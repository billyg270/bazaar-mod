package uk.co.cablepost.bazaar.block.blockentity;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.bazaar.mixin.FlowerPotBlockAccessor;
import uk.co.cablepost.bazaar.util.MiniMadnessBlockEntityBaseWithInventory;

public class BazaarFlowerPotBlockEntity extends MiniMadnessBlockEntityBaseWithInventory {

    public boolean empty = false;

    public BazaarFlowerPotBlockEntity(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state, 1);
    }

    @Override
    public void MmReadNbtI(NbtCompound nbt) {
        empty = nbt.getBoolean("empty");
    }

    @Override
    public void MmWriteNbtI(NbtCompound nbt) {
        nbt.putBoolean("empty", empty);
    }

    //@Nullable
    //@Override
    //public Packet<ClientPlayPacketListener> toUpdatePacket() {
    //    return BlockEntityUpdateS2CPacket.create(this);
    //}

    //@Override
    //public NbtCompound toInitialChunkDataNbt() {
    //    return createNbt();
    //}

    @Override
    public boolean canInsert(int slot, ItemStack stack, @Nullable Direction dir) {
        if(!getStack(slot).isEmpty()){
            return false;
        }

        for(Block b : FlowerPotBlockAccessor.getContentToPotted().keySet()){
            if(stack.getItem() == b.asItem()){
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean canExtract(int slot, ItemStack stack, Direction dir) {
        return true;
    }

    public static void clientTick(World world, BlockPos blockPos, BlockState blockState, BazaarFlowerPotBlockEntity blockEntity) {
    }

    public static void serverTick(World world, BlockPos blockPos, BlockState blockState, BazaarFlowerPotBlockEntity blockEntity) {
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        ItemStack toRet = super.removeStack(slot, amount);
        updateEmpty();
        markDirty();
        return toRet;
    }

    @Override
    public ItemStack removeStack(int slot) {
        ItemStack toRet = super.removeStack(slot);
        updateEmpty();
        markDirty();
        return toRet;
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        super.setStack(slot, stack);
        updateEmpty();
        markDirty();
    }

    //@Override
    //public void markDirty() {
    //    super.markDirty();
    //    if (this.hasWorld() && !this.getWorld().isClient()) {
    //        ((ServerWorld) world).getChunkManager().markForUpdate(getPos());
    //    }
    //}

    public void updateEmpty(){
        empty = getStack(0).isEmpty();;
    }
}
