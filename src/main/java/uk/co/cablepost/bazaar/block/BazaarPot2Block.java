package uk.co.cablepost.bazaar.block;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.bazaar.Bazaar;
import uk.co.cablepost.bazaar.block.blockentity.BazaarFlowerPotBlockEntity;
import uk.co.cablepost.bazaar.block.blockentity.BazaarPot2BlockEntity;

public class BazaarPot2Block extends BazaarFlowerPotBlock {
    public BazaarPot2Block(Settings settings) {
        super(settings);
    }

    @Nullable
    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new BazaarPot2BlockEntity(pos, state);
    }

    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return world.isClient ?
                checkType(type, Bazaar.BAZAAR_POT_2.BLOCK_ENTITY, BazaarFlowerPotBlockEntity::clientTick) :
                checkType(type, Bazaar.BAZAAR_POT_2.BLOCK_ENTITY, BazaarFlowerPotBlockEntity::serverTick)
        ;
    }
}
