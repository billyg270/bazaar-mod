package uk.co.cablepost.bazaar.block;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.HorizontalFacingBlock;
import net.minecraft.block.ShapeContext;
import net.minecraft.item.ItemPlacementContext;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.Properties;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import net.minecraft.world.WorldAccess;
import net.minecraft.world.WorldView;

public class FlagBlock extends HorizontalFacingBlock {
    public static final BooleanProperty CONNECTED_LEFT = BooleanProperty.of("connected_left");
    public static final BooleanProperty CONNECTED_RIGHT = BooleanProperty.of("connected_right");

    public FlagBlock(Settings settings) {
        super(settings);
        setDefaultState(
                this.stateManager.getDefaultState()
                .with(Properties.HORIZONTAL_FACING, Direction.NORTH)
                .with(CONNECTED_LEFT, Boolean.FALSE)
                .with(CONNECTED_RIGHT, Boolean.FALSE)
        );
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> stateManager) {
        stateManager.add(Properties.HORIZONTAL_FACING);
        stateManager.add(CONNECTED_LEFT);
        stateManager.add(CONNECTED_RIGHT);
    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView view, BlockPos pos, ShapeContext ctx) {
        Direction dir = state.get(FACING);

        float thickness = 0.1f;//0.06 to match the wood

        return switch (dir) {
            case NORTH -> VoxelShapes.cuboid(0.0f, 0.2f, 1f - thickness, 1.0f, 1.0f, 1.0f);
            case SOUTH -> VoxelShapes.cuboid(0.0f, 0.2f, 0.0f, 1.0f, 1.0f, thickness);
            case EAST -> VoxelShapes.cuboid(0.0f, 0.2f, 0.0f, thickness, 1.0f, 1.0f);
            case WEST -> VoxelShapes.cuboid(1f - thickness, 0.2f, 0.0f, 1.0f, 1.0f, 1.0f);
            default -> VoxelShapes.fullCube();
        };
    }

    @Override
    public boolean canPlaceAt(BlockState state, WorldView world, BlockPos pos) {
        if(!world.getBlockState(pos.down()).isAir()){
            return false;
        }

        return super.canPlaceAt(state, world, pos);
    }

    @Override
    public BlockState getPlacementState(ItemPlacementContext ctx) {
        World world = ctx.getWorld();

        BlockState state = getDefaultState().with(Properties.HORIZONTAL_FACING, ctx.getPlayerFacing().getOpposite());

        BlockState stateLeft = world.getBlockState(getBlockPosOnSide(state, ctx.getBlockPos(), true));
        BlockState stateRight = world.getBlockState(getBlockPosOnSide(state, ctx.getBlockPos(), false));



        return state
            .with(
                CONNECTED_LEFT,
                (
                    stateLeft.getBlock() instanceof FlagBlock &&
                    stateLeft.get(Properties.HORIZONTAL_FACING).asRotation() == state.get(Properties.HORIZONTAL_FACING).asRotation()
                )
            )
            .with(
                CONNECTED_RIGHT,
                (
                    stateRight.getBlock() instanceof FlagBlock &&
                    stateRight.get(Properties.HORIZONTAL_FACING).asRotation() == state.get(Properties.HORIZONTAL_FACING).asRotation()
                )
            )
        ;
    }

    @Override
    public BlockState getStateForNeighborUpdate(BlockState state, Direction direction, BlockState neighborState, WorldAccess world, BlockPos pos, BlockPos neighborPos) {
        if(neighborPos.getManhattanDistance(getBlockPosOnSide(state, pos, true)) < 1){
            return state.with(
                    CONNECTED_LEFT,
                    (
                            neighborState.getBlock() instanceof FlagBlock &&
                            neighborState.get(Properties.HORIZONTAL_FACING).asRotation() == state.get(Properties.HORIZONTAL_FACING).asRotation()
                    )
            );
        }

        if(neighborPos.getManhattanDistance(getBlockPosOnSide(state, pos, false)) < 1){
            return state.with(
                    CONNECTED_RIGHT,
                    (
                            neighborState.getBlock() instanceof FlagBlock &&
                            neighborState.get(Properties.HORIZONTAL_FACING).asRotation() == state.get(Properties.HORIZONTAL_FACING).asRotation()
                    )
            );
        }

        return state;
    }

    private BlockPos getBlockPosOnSide(BlockState state, BlockPos blockPosSelf, boolean leftSide){
        if(leftSide){
            return blockPosSelf.add(state.get(Properties.HORIZONTAL_FACING).rotateClockwise(Direction.Axis.Y).getVector());
        }
        else{
            return blockPosSelf.add(state.get(Properties.HORIZONTAL_FACING).rotateCounterclockwise(Direction.Axis.Y).getVector());
        }
    }
}
