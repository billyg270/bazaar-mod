package uk.co.cablepost.bazaar.block;

import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.BlockWithEntity;
import net.minecraft.block.ShapeContext;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import uk.co.cablepost.bazaar.block.blockentity.BazaarFlowerPotBlockEntity;

public class BazaarFlowerPotBlock extends BlockWithEntity {
    public BazaarFlowerPotBlock(Settings settings) {
        super(settings);
    }

    @Nullable
    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return null;
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.MODEL;
    }

    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return null;
    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView view, BlockPos pos, ShapeContext ctx) {
        return VoxelShapes.cuboid(0.25f, 0.0f, 0.25f, 0.75f, 0.5f, 0.75f);
    }

    @Override
    public ActionResult onUse(BlockState blockState, World world, BlockPos blockPos, PlayerEntity player, Hand hand, BlockHitResult blockHitResult) {
        if (world.isClient) return ActionResult.SUCCESS;
        BazaarFlowerPotBlockEntity blockEntity = (BazaarFlowerPotBlockEntity) world.getBlockEntity(blockPos);

        assert blockEntity != null;

        if(!blockEntity.getStack(0).isEmpty()){
            if(blockEntity.getStack(0).isEmpty()){
                return ActionResult.SUCCESS;
            }

            //ItemScatterer.spawn(world, blockPos, blockEntity);
            player.getInventory().offerOrDrop(blockEntity.getStack(0));
            blockEntity.removeStack(0);

            return ActionResult.SUCCESS;
        }

        if (player.getStackInHand(hand).isEmpty()) {
            return ActionResult.SUCCESS;
        }

        ItemStack stack = player.getStackInHand(hand).copy();

        if(!blockEntity.canInsert(0, stack, Direction.UP)){
            return ActionResult.SUCCESS;
        }

        int oldCount = stack.getCount();
        stack.setCount(1);
        blockEntity.setStack(0, stack);
        player.getStackInHand(hand).setCount(oldCount - 1);

        return ActionResult.SUCCESS;
    }

    @Override
    public void onStateReplaced(BlockState state, World world, BlockPos pos, BlockState newState, boolean moved) {
        if (state.getBlock() != newState.getBlock()) {
            BlockEntity blockEntity = world.getBlockEntity(pos);
            if (blockEntity instanceof BazaarFlowerPotBlockEntity blockEntity1) {
                ItemScatterer.spawn(world, pos, blockEntity1);
                world.updateComparators(pos,this);
            }
            super.onStateReplaced(state, world, pos, newState, moved);
        }
    }
}
