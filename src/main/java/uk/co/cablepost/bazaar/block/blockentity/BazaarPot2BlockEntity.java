package uk.co.cablepost.bazaar.block.blockentity;

import net.minecraft.block.BlockState;
import net.minecraft.util.math.BlockPos;
import uk.co.cablepost.bazaar.Bazaar;
import uk.co.cablepost.bazaar.block.blockentity.BazaarFlowerPotBlockEntity;

public class BazaarPot2BlockEntity extends BazaarFlowerPotBlockEntity {
    public BazaarPot2BlockEntity(BlockPos pos, BlockState state) {
        super(Bazaar.BAZAAR_POT_2.BLOCK_ENTITY, pos, state);
    }
}
