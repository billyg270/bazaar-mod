package uk.co.cablepost.bazaar.entity.variant;

import java.util.Arrays;
import java.util.Comparator;

public enum CameleonVariant {
    GREEN(0),
    BROWN(1),
    BLUE(2),
    RED(3),
    BLACK(4);

    private static final CameleonVariant[] BY_ID = Arrays.stream(values()).sorted(Comparator.comparingInt(CameleonVariant::getId)).toArray(CameleonVariant[]::new);
    private final int id;
    CameleonVariant(int id) {
        this.id = id;
    }

    public int getId() { return this.id; }

    public static CameleonVariant byId(int id) { return BY_ID[id % BY_ID.length]; }
}
