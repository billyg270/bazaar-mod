package uk.co.cablepost.bazaar.entity;

import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import uk.co.cablepost.bazaar.Bazaar;
import uk.co.cablepost.bazaar.client.entity.renderer.CameleonEntityRenderer;
import uk.co.cablepost.bazaar.entity.custom.CameleonEntity;

public class BazaarEntityRegister {
    public static final EntityType<CameleonEntity> CAMELEON = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(Bazaar.MOD_ID, "cameleon"),
            FabricEntityTypeBuilder.create(SpawnGroup.CREATURE, CameleonEntity::new).dimensions(EntityDimensions.fixed(2.5f,1.5f)).build()
    );

    public static void Register() {
        FabricDefaultAttributeRegistry.register(CAMELEON, CameleonEntity.setAttributes());
    }

    public static void RegisterClient() {
        EntityRendererRegistry.register(CAMELEON, CameleonEntityRenderer::new);
    }
}
