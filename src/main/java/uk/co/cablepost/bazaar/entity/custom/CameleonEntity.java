package uk.co.cablepost.bazaar.entity.custom;

import com.mojang.logging.LogUtils;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.block.*;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.FuzzyTargeting;
import net.minecraft.entity.ai.goal.*;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.entity.passive.TameableEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Util;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.ChunkSectionPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.*;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.ChunkStatus;
import net.minecraft.world.event.GameEvent;
import org.slf4j.Logger;
import software.bernie.geckolib3.core.AnimationState;
import software.bernie.geckolib3.core.IAnimatable;
import software.bernie.geckolib3.core.PlayState;
import software.bernie.geckolib3.core.builder.AnimationBuilder;
import software.bernie.geckolib3.core.builder.ILoopType;
import software.bernie.geckolib3.core.controller.AnimationController;
import software.bernie.geckolib3.core.event.predicate.AnimationEvent;
import software.bernie.geckolib3.core.manager.AnimationData;
import software.bernie.geckolib3.core.manager.AnimationFactory;
import software.bernie.geckolib3.util.GeckoLibUtil;
import uk.co.cablepost.bazaar.Networking.BazaarNetworkingRegistry;
import uk.co.cablepost.bazaar.entity.variant.CameleonVariant;
import uk.co.cablepost.bazaar.sounds.BazaarSoundRegister;
import uk.co.cablepost.bazaar.util.Utils;

import javax.annotation.Nullable;

public class CameleonEntity extends TameableEntity implements IAnimatable {
    private static final Logger field_36332 = LogUtils.getLogger();
    private static final float speed = 0.3f;
    //private boolean justAte = false;
    private NbtCompound persistantData = new NbtCompound();
    public String pDatS = "bazaar.compData";
    //private int ambientDarkness;
    private final AnimationFactory factory = GeckoLibUtil.createFactory(this);
    private boolean needEat = false;
    private int eatWaitCooldown = 0;

    public CameleonEntity(EntityType<? extends TameableEntity> entityType, World world) {
        super(entityType, world);
    }

    public void initGoals() {
        this.goalSelector.add(0, new SwimGoal(this));
        this.goalSelector.add(1, new CameleonWanderFarGoal(this, speed));
        this.goalSelector.add(2, new CameleonEatGrassGoal(this, speed, 10));
        this.goalSelector.add(3, new LookAroundGoal(this));// pivot point not set right and tongue doesn't rotate with head. Fix first or remove goal
    }

    private<E extends IAnimatable> PlayState idlePredicate(AnimationEvent<E> event) {
        boolean isMov = this.isMoving();
        if(isDead()) return PlayState.STOP;
        if(!isMov) {
            if(this.isAwake()) event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.cameleon.idle_awake", ILoopType.EDefaultLoopTypes.LOOP));
            else event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.cameleon.idle_sleep", ILoopType.EDefaultLoopTypes.LOOP));
        } else event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.cameleon.runner", ILoopType.EDefaultLoopTypes.LOOP));

        return PlayState.CONTINUE;
    }

    private<E extends IAnimatable> PlayState wsPredicate(AnimationEvent<E> event) {
        if(event.getController().getAnimationState() != AnimationState.Stopped) return PlayState.CONTINUE;
        boolean isday = this.isDay();

        boolean isnight = this.isNight();
        if(isDead()) return PlayState.STOP;
        if(isday && !isAwake()) {
            this.dataTracker.set(AWAKE, true);

            event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.cameleon.wakeup", ILoopType.EDefaultLoopTypes.PLAY_ONCE));
            return PlayState.CONTINUE;
        }else if(isnight && isAwake()) {
            this.dataTracker.set(AWAKE, false);
            event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.cameleon.gosleep", ILoopType.EDefaultLoopTypes.PLAY_ONCE));
            return PlayState.CONTINUE;
        }
        //System.out.println(this.dataTracker.get(AWAKE));
        return PlayState.STOP;
    }
    private <E extends IAnimatable> PlayState deathPredicate(AnimationEvent<E> event) {
        if(this.isDead())
        {
            event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.cameleon.death", ILoopType.EDefaultLoopTypes.HOLD_ON_LAST_FRAME));
            return PlayState.CONTINUE;
        }
        return PlayState.STOP;
    }
    public void EatPlant(BlockPos block) {
        this.dataTracker.set(JUST_ATE, true);
        needEat = true;
        eatWaitCooldown = 20;
        PacketByteBuf buf = PacketByteBufs.create();
        buf.writeInt(this.getId());
        for(PlayerEntity player : this.world.getPlayers()) {
            ServerPlayNetworking.send((ServerPlayerEntity) player, BazaarNetworkingRegistry.CAMELEON_MUNCH_ID, buf);
        }
        //System.out.println("Eat Called");
        System.out.println("Ate Plant");
        if(world.getBlockState(block).getBlock() instanceof PlantBlock) {
            world.breakBlock(block, false);
        }
    }
    private<E extends IAnimatable> PlayState munchPredicate(AnimationEvent<E> event) {
        if(this.dataTracker.get(JUST_ATE)) {
            event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.cameleon.munch", ILoopType.EDefaultLoopTypes.PLAY_ONCE));
        }
        if(event.getController().getAnimationState() == AnimationState.Stopped && this.dataTracker.get(JUST_ATE)) {
            setAte(false);
            event.getController().markNeedsReload();
        }
        return PlayState.CONTINUE;
    }
    @Override
    public void onDeath(DamageSource damageSource) {
        if (!this.isRemoved() && !this.dead) {
            Entity entity = damageSource.getAttacker();
            LivingEntity livingEntity = this.getPrimeAdversary();
            if (this.scoreAmount >= 0 && livingEntity != null) {
                livingEntity.updateKilledAdvancementCriterion(this, this.scoreAmount, damageSource);
            }

            if (this.isSleeping()) {
                this.wakeUp();
            }

            if (!this.world.isClient && this.hasCustomName()) {
                field_36332.info("Named entity {} died: {}", this, this.getDamageTracker().getDeathMessage().getString());
            }

            this.dead = true;
            this.getDamageTracker().update();
            if (this.world instanceof ServerWorld) {
                if (entity == null || entity.onKilledOther((ServerWorld)this.world, this)) {
                    this.emitGameEvent(GameEvent.ENTITY_DIE);
                    this.drop(damageSource);
                    this.onKilledBy(livingEntity);
                }

                this.world.sendEntityStatus(this, (byte)3);
            }

            //this.setPose(EntityPose.DYING);
        }
    }
    private boolean isDay() {
        return !this.world.getDimension().hasFixedTime() && this.dataTracker.get(AMBIENT_DARKNESS) < 7;
    }
    private boolean isNight() {
        return !this.world.getDimension().hasFixedTime() && !this.isDay();
    }
    @Override
    public void writeCustomDataToNbt(NbtCompound nbt) {
        if(!nbt.contains(pDatS)) nbt.put(pDatS, persistantData);
        persistantData.putInt("Variant", this.getTypeVariant());
        persistantData.putBoolean("JustAte", this.dataTracker.get(JUST_ATE));
        super.writeCustomDataToNbt(nbt);
    }
    public void setAte(boolean val) {
        this.dataTracker.set(JUST_ATE, val);
    }


    @Override
    public void readCustomDataFromNbt(NbtCompound nbt) {
        if(nbt.contains(pDatS)) persistantData = nbt.getCompound(pDatS);
        if(persistantData != null) {
            if(persistantData.contains("Variant")) this.dataTracker.set(DATA_ID_TYPE_VARIANT, persistantData.getInt("Variant"));
            if(persistantData.contains("JustAte")) this.dataTracker.set(JUST_ATE, persistantData.getBoolean("JustAte"));
        }
        super.readCustomDataFromNbt(nbt);
    }
    @Override
    public void registerControllers(AnimationData data) {
        data.addAnimationController(new AnimationController<>(this, "idleCon",0, this::idlePredicate));
        data.addAnimationController(new AnimationController<>(this, "wsCon", 0, this::wsPredicate));
        data.addAnimationController(new AnimationController<>(this, "deathCon", 0, this::deathPredicate));
        data.addAnimationController(new AnimationController<>(this, "munchCon", 0, this::munchPredicate));
    }

    public static DefaultAttributeContainer.Builder setAttributes() {

        return PassiveEntity.createMobAttributes()
                .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, speed)
                .add(EntityAttributes.GENERIC_KNOCKBACK_RESISTANCE, 5.0f)
                .add(EntityAttributes.GENERIC_MAX_HEALTH, 40f);
    }
    @Override
    public AnimationFactory getFactory() {
        return factory;
    }

    private boolean isMoving() {
        return !(this.getVelocity().x == 0 && this.getVelocity().z == 0);
    }

    public boolean isAwake() {
        //System.out.println(this.dataTracker.get(AWAKE));
        return this.dataTracker.get(AWAKE);
    }

    public void tick() {
        super.tick();

        if(!this.world.isClient) {
            int darkness = this.world.getAmbientDarkness();
            //System.out.println(darkness);
            this.dataTracker.set(AMBIENT_DARKNESS, darkness);

            if(isDay() && !this.dataTracker.get(AWAKE)) {
                this.dataTracker.set(AWAKE, true);
                this.playSound(BazaarSoundRegister.CAMELEON_WAKEUP, 5.0f, 1);
            } else if(isNight() && this.dataTracker.get(AWAKE)) {
                this.dataTracker.set(AWAKE, false);
            }
            if(needEat) {
                if(eatWaitCooldown == 0) {

                    eatWaitCooldown = -1;
                    needEat = false;
                } else if(eatWaitCooldown != -1)eatWaitCooldown--;
            }
        }
    }
    @Override
    protected void initDataTracker() {
        super.initDataTracker();
        this.dataTracker.startTracking(DATA_ID_TYPE_VARIANT, 0);
        this.dataTracker.startTracking(AMBIENT_DARKNESS, 0);
        this.dataTracker.startTracking(AWAKE, !this.world.isClient() && this.world.isDay());
        this.dataTracker.startTracking(JUST_ATE, false);
    }

    private static final TrackedData<Integer> AMBIENT_DARKNESS =
            DataTracker.registerData(CameleonEntity.class, TrackedDataHandlerRegistry.INTEGER);
    private static final TrackedData<Boolean> AWAKE =
            DataTracker.registerData(CameleonEntity.class, TrackedDataHandlerRegistry.BOOLEAN);
    private static final TrackedData<Boolean> JUST_ATE =
            DataTracker.registerData(CameleonEntity.class, TrackedDataHandlerRegistry.BOOLEAN);
    // -- Variant Stuff --
    private static final TrackedData<Integer> DATA_ID_TYPE_VARIANT =
            DataTracker.registerData(CameleonEntity.class, TrackedDataHandlerRegistry.INTEGER);

    public EntityData initialize(ServerWorldAccess world, LocalDifficulty difficulty, SpawnReason spawnReason, @Nullable EntityData entityData, @Nullable NbtCompound entityNbt) {
        CameleonVariant variant = Util.getRandom(CameleonVariant.values(), this.random);
        setVariant(variant);
        return super.initialize(world, difficulty, spawnReason, entityData, entityNbt);
    }

    @org.jetbrains.annotations.Nullable
    @Override
    public PassiveEntity createChild(ServerWorld world, PassiveEntity entity) {
        return null;
    }

    public CameleonVariant getVariant() {
        return CameleonVariant.byId(this.getTypeVariant() & 255);
    }

    private int getTypeVariant() {
        return this.dataTracker.get(DATA_ID_TYPE_VARIANT);
    }

    private void setVariant(CameleonVariant variant) {
        this.dataTracker.set(DATA_ID_TYPE_VARIANT, variant.getId() & 255);
    }

    @Override
    protected SoundEvent getAmbientSound(){return !isAwake() ? BazaarSoundRegister.CAMELEON_SLEEP : null;}
    @Override
    protected SoundEvent getHurtSound(DamageSource source){return null;}
    @Override
    protected SoundEvent getDeathSound(){return BazaarSoundRegister.CAMELEON_DEATH;}
    @Override
    protected void playStepSound(BlockPos pos, BlockState state) {
        this.playSound(SoundEvents.ENTITY_IRON_GOLEM_STEP, 1.0F, 1.0F);
    }



}

class CameleonWanderFarGoal extends WanderAroundFarGoal {
    public static final float CHANCE = 0.001F;
    protected final float probability;
    private final CameleonEntity self;
    public CameleonWanderFarGoal(CameleonEntity pathAwareEntity, double d) {
        this(pathAwareEntity, d, CHANCE);
    }

    public CameleonWanderFarGoal(CameleonEntity mob, double speed, float probability) {
        super(mob, speed);
        this.probability = probability;
        self = mob;
    }
    @Override
    public boolean canStart() {
        //System.out.println(super.canStart() && !this.self.world.isClient() && this.self.world.isDay());
        return super.canStart() && !this.self.world.isClient() && this.self.world.isDay();
    }
    @Override
    public boolean shouldContinue() {
        return super.shouldContinue() && self.isAwake();
    }
    @org.jetbrains.annotations.Nullable
    protected Vec3d getWanderTarget() {
        if (this.mob.isInsideWaterOrBubbleColumn()) {
            Vec3d vec3d = FuzzyTargeting.find(this.mob, 15, 7);
            return vec3d == null ? super.getWanderTarget() : vec3d;
        } else {
            return this.mob.getRandom().nextFloat() >= this.probability ? FuzzyTargeting.find(this.mob, 10, 7) : super.getWanderTarget();
        }
    }

}

class CameleonEatGrassGoal extends MoveToTargetPosGoal {
    private static CameleonEntity mob;
    public CameleonEatGrassGoal (CameleonEntity mob, double speed, int range) {
        super(mob, speed, range);
        this.mob = mob;
    }
    //BlockPos targetBlock;
    @Override
    public boolean canStart() {
        return super.canStart() && mob.world.getGameRules().getBoolean(GameRules.DO_MOB_GRIEFING) && mob.isAwake();
    }
    @Override
    public void start() {
        super.start();
        //targetBlock = getTargetPos();
    }
    public void stop() {
        super.stop();
        System.out.println("Stopped");
        System.out.println(Utils.distanceTo(mob.getBlockPos(), targetPos));
        if(Utils.distanceTo(mob.getBlockPos(), targetPos) <= 3) {
            mob.EatPlant(targetPos);
        }
    }
    @Override
    protected boolean isTargetPos(WorldView world, BlockPos pos) {
        Chunk chunk = world.getChunk(ChunkSectionPos.getSectionCoord(pos.getX()), ChunkSectionPos.getSectionCoord(pos.getZ()), ChunkStatus.FULL, false);
        if (chunk != null) {
            return
                    (chunk.getBlockState(pos).getBlock() instanceof PlantBlock) &&
                            chunk.getBlockState(pos.up(2)).isAir() &&
                            chunk.getBlockState(pos.up(3)).isAir() &&
                            chunk.getBlockState(pos.up(4)).isAir()
                    ;
        }
        return false;
    }


}
