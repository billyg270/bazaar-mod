package uk.co.cablepost.bazaar.entity.ai.goal;

import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.ai.goal.MeleeAttackGoal;
import net.minecraft.entity.mob.PathAwareEntity;

public class ChonkerMeleeAttackGoal extends MeleeAttackGoal {
    public ChonkerMeleeAttackGoal(PathAwareEntity mob, double speed, boolean pauseWhenMobIdle) {
        super(mob, speed, pauseWhenMobIdle);
    }

    @Override
    protected double getSquaredMaxAttackDistance(LivingEntity entity) {
        return 5f + entity.getWidth();
    }
}
