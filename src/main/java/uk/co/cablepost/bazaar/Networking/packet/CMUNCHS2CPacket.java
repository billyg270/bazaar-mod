package uk.co.cablepost.bazaar.Networking.packet;

import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.network.PacketByteBuf;
import uk.co.cablepost.bazaar.entity.custom.CameleonEntity;


public class CMUNCHS2CPacket {
    public static void Receive(MinecraftClient minecraftClient, ClientPlayNetworkHandler clientPlayNetworkHandler, PacketByteBuf packetByteBuf, PacketSender packetSender) {
        System.out.println("Received Packet");
        assert minecraftClient.world != null;
        if(minecraftClient.world.getEntityById(packetByteBuf.getInt(0)) instanceof CameleonEntity cl) cl.setAte(true);

    }
}
