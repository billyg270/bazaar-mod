package uk.co.cablepost.bazaar.Networking;

import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.minecraft.util.Identifier;
import uk.co.cablepost.bazaar.Bazaar;
import uk.co.cablepost.bazaar.Networking.packet.CMUNCHS2CPacket;

public class BazaarNetworkingRegistry {
    public static final Identifier CAMELEON_MUNCH_ID = new Identifier(Bazaar.MOD_ID, "cameleon_munch");

    public static void RegisterC2SPackets() {

    }

    public static void RegisterS2CPackets() {
        ClientPlayNetworking.registerGlobalReceiver(CAMELEON_MUNCH_ID, CMUNCHS2CPacket::Receive);
    }
}
