package uk.co.cablepost.bazaar.util;

import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.Block;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.Identifier;
import uk.co.cablepost.bazaar.Bazaar;

public class MiniMadnessBlockEntity<T extends BlockEntity> {
    public final Identifier IDENTIFIER;
    public final Block BLOCK;
    public final BlockEntityType<T> BLOCK_ENTITY;

    public MiniMadnessBlockEntity(Identifier identifier, Block block, FabricBlockEntityTypeBuilder.Factory<T> blockEntityFactory) {
        IDENTIFIER = identifier;
        BLOCK = block;
        BLOCK_ENTITY = Bazaar.registerBlockEntity(
                identifier,
                block,
                blockEntityFactory
        );
    }
}
