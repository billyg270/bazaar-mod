package uk.co.cablepost.bazaar.util;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.Packet;
import net.minecraft.network.listener.ClientPlayPacketListener;
import net.minecraft.network.packet.s2c.play.BlockEntityUpdateS2CPacket;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public abstract class MiniMadnessBlockEntityBase extends BlockEntity {
    public MiniMadnessBlockEntityBase(BlockEntityType<?> type, BlockPos pos, BlockState state) {
        super(type, pos, state);
    }

    public abstract void MmReadNbt(NbtCompound nbt);
    public abstract void MmWriteNbt(NbtCompound nbt);

    @Override
    public final void writeNbt(NbtCompound nbt) {
        MmWriteNbt(nbt);
        super.writeNbt(nbt);
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);
        MmReadNbt(nbt);
    }

    @Nullable
    @Override
    public Packet<ClientPlayPacketListener> toUpdatePacket() {
        return BlockEntityUpdateS2CPacket.create(this);
    }

    @Override
    public NbtCompound toInitialChunkDataNbt() {
        return createNbt();
    }

    @Override
    public void markDirty() {
        boolean serverSide = this.hasWorld() && !Objects.requireNonNull(this.getWorld()).isClient();

        super.markDirty();

        if (serverSide) {
            assert world != null;
            ((ServerWorld) world).getChunkManager().markForUpdate(getPos());
        }
    }
}
