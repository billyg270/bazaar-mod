package uk.co.cablepost.bazaar.util;

import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.SidedInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

import java.util.Objects;

public abstract class MiniMadnessBlockEntityBaseWithInventory extends MiniMadnessBlockEntityBase implements SidedInventory {

    public int inventorySize;
    protected DefaultedList<ItemStack> inventory;

    public MiniMadnessBlockEntityBaseWithInventory(BlockEntityType<?> type, BlockPos pos, BlockState state, int inventorySize) {
        super(type, pos, state);
        this.inventorySize = inventorySize;
        inventory = DefaultedList.ofSize(this.inventorySize, ItemStack.EMPTY);
    }

    public abstract void MmReadNbtI(NbtCompound nbt);
    public abstract void MmWriteNbtI(NbtCompound nbt);

    @Override
    public final void MmReadNbt(NbtCompound nbt) {
        MmReadNbtI(nbt);
        Inventories.readNbt(nbt, inventory);
    }

    @Override
    public final void MmWriteNbt(NbtCompound nbt) {
        MmWriteNbtI(nbt);
        Inventories.writeNbt(nbt, inventory);
    }

    @Override
    public int[] getAvailableSlots(Direction side) {
        int[] result = new int[inventory.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = i;
        }

        return result;
    }

    @Override
    public int size() {
        return inventory.size();
    }

    @Override
    public boolean isEmpty() {
        for (ItemStack itemStack : inventory) {
            if (!itemStack.isEmpty()) {
                return false;
            }
        }

        return true;
    }

    @Override
    public ItemStack getStack(int slot) {
        return inventory.get(slot);
    }

    @Override
    public ItemStack removeStack(int slot, int amount) {
        if(amount == 0){
            return ItemStack.EMPTY;
        }

        if(amount == inventory.get(slot).getCount()){
            return removeStack(slot);
        }

        ItemStack toRet = inventory.get(slot).copy();
        inventory.get(slot).decrement(amount);
        toRet.setCount(amount);

        return toRet;
    }

    @Override
    public ItemStack removeStack(int slot) {
        ItemStack toRet = inventory.get(slot).copy();
        inventory.set(slot, ItemStack.EMPTY);

        return toRet;
    }

    @Override
    public void setStack(int slot, ItemStack stack) {
        inventory.set(slot, stack);
    }

    @Override
    public boolean canPlayerUse(PlayerEntity player) {
        return true;
    }

    @Override
    public void clear() {
        inventory.clear();
    }

    @Override
    public void markDirty() {
        boolean serverSide = this.hasWorld() && !Objects.requireNonNull(this.getWorld()).isClient();

        super.markDirty();

        if (serverSide) {
            assert world != null;
            ((ServerWorld) world).getChunkManager().markForUpdate(getPos());
        }
    }
}
