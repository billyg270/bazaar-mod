package uk.co.cablepost.bazaar.sounds;

import net.minecraft.sound.SoundEvent;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import uk.co.cablepost.bazaar.Bazaar;


public class BazaarSoundRegister {
    //Cameleon Sounds
    private static final Identifier cameleon_attack = new Identifier(Bazaar.MOD_ID,"attack");
    public static SoundEvent CAMELEON_ATTACK = new SoundEvent(cameleon_attack);
    private static final Identifier cameleon_death = new Identifier(Bazaar.MOD_ID, "death");
    public static SoundEvent CAMELEON_DEATH = new SoundEvent(cameleon_death);
    private static final Identifier cameleon_lick = new Identifier(Bazaar.MOD_ID, "lick");
    public static SoundEvent CAMELEON_LICK = new SoundEvent(cameleon_lick);
    private static final Identifier cameleon_sleep = new Identifier(Bazaar.MOD_ID, "sleep");
    public static SoundEvent CAMELEON_SLEEP = new SoundEvent(cameleon_sleep);
    private static final Identifier cameleon_stomp = new Identifier(Bazaar.MOD_ID, "stomp");
    public static SoundEvent CAMELEON_STOMP = new SoundEvent(cameleon_stomp);
    private static final Identifier cameleon_wakeup = new Identifier(Bazaar.MOD_ID, "wakeup");
    public static SoundEvent CAMELEON_WAKEUP = new SoundEvent(cameleon_wakeup);
    private static final Identifier cameleon_walk = new Identifier(Bazaar.MOD_ID, "walk");
    public static SoundEvent CAMELEON_WALK = new SoundEvent(cameleon_walk);

    //------
    public static void Register() {
        Registry.register(Registry.SOUND_EVENT, cameleon_attack, CAMELEON_ATTACK);
        Registry.register(Registry.SOUND_EVENT, cameleon_death, CAMELEON_DEATH);
        Registry.register(Registry.SOUND_EVENT, cameleon_lick, CAMELEON_LICK);
        Registry.register(Registry.SOUND_EVENT, cameleon_sleep, CAMELEON_SLEEP);
        Registry.register(Registry.SOUND_EVENT, cameleon_stomp, CAMELEON_STOMP);
        Registry.register(Registry.SOUND_EVENT, cameleon_wakeup, CAMELEON_WAKEUP);
        Registry.register(Registry.SOUND_EVENT, cameleon_walk, CAMELEON_WALK);


    }
}
