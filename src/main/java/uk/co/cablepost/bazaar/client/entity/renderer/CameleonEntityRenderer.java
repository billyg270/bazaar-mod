package uk.co.cablepost.bazaar.client.entity.renderer;

import com.google.common.collect.Maps;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.util.Identifier;
import net.minecraft.util.Util;
import software.bernie.geckolib3.renderers.geo.GeoEntityRenderer;
import uk.co.cablepost.bazaar.Bazaar;
import uk.co.cablepost.bazaar.client.entity.model.CameleonEntityModel;
import uk.co.cablepost.bazaar.entity.custom.CameleonEntity;
import uk.co.cablepost.bazaar.entity.variant.CameleonVariant;

import java.util.Map;

public class CameleonEntityRenderer extends GeoEntityRenderer<CameleonEntity> {
    public static final Map<CameleonVariant, Identifier> LOCATION_BY_VARIANT =
            Util.make(Maps.newEnumMap(CameleonVariant.class), (map) -> {
                map.put(CameleonVariant.GREEN, new Identifier(Bazaar.MOD_ID, "textures/entity/cameleon/cameleon_texture.png"));
                map.put(CameleonVariant.BROWN, new Identifier(Bazaar.MOD_ID, "textures/entity/cameleon/cameleon_texture2.png"));
                map.put(CameleonVariant.BLUE, new Identifier(Bazaar.MOD_ID, "textures/entity/cameleon/cameleon_texture3.png"));
                map.put(CameleonVariant.RED, new Identifier(Bazaar.MOD_ID, "textures/entity/cameleon/cameleon_texture4.png"));
                map.put(CameleonVariant.BLACK, new Identifier(Bazaar.MOD_ID, "textures/entity/cameleon/cameleon_texture5.png"));
            });

    public CameleonEntityRenderer(EntityRendererFactory.Context renderManager) {
        super(renderManager, new CameleonEntityModel());
    }

    public Identifier getTextureLocation(CameleonEntity instance) {
        return LOCATION_BY_VARIANT.get(instance.getVariant());
    }
    @Override
    protected float getDeathMaxRotation(CameleonEntity animatable) {
        return 0f;
    }
/*
    @Override
    public int getOverlay(CameleonEntity entity, float u) {
        return OverlayTexture.packUv(OverlayTexture.getU(u),       OverlayTexture.getV(false));
        //return -1;
    }
*/
}
