package uk.co.cablepost.bazaar.client.entity.model;

import net.minecraft.util.Identifier;
import software.bernie.geckolib3.core.event.predicate.AnimationEvent;
import software.bernie.geckolib3.core.processor.IBone;
import software.bernie.geckolib3.model.AnimatedGeoModel;
import software.bernie.geckolib3.model.provider.data.EntityModelData;
import uk.co.cablepost.bazaar.Bazaar;
import uk.co.cablepost.bazaar.client.entity.renderer.CameleonEntityRenderer;
import uk.co.cablepost.bazaar.entity.custom.CameleonEntity;

public class CameleonEntityModel extends AnimatedGeoModel<CameleonEntity> {
    private static final Identifier modelResource = new Identifier(Bazaar.MOD_ID,"geo/cameleon_model.geo.json");
    private static final Identifier animationResource = new Identifier(Bazaar.MOD_ID, "animations/cameleon.animation.json");

    @Override
    public Identifier getModelResource(CameleonEntity object) {
        return modelResource;
    }

    @Override
    public Identifier getTextureResource(CameleonEntity object) {
        return CameleonEntityRenderer.LOCATION_BY_VARIANT.get(object.getVariant());
    }

    @Override
    public Identifier getAnimationResource(CameleonEntity animatable) {
        return animationResource;
    }

    public void setLivingAnimations(CameleonEntity entity, Integer uniqueID, AnimationEvent customPredicate) {
        super.setLivingAnimations(entity, uniqueID, customPredicate);
        IBone head = this.getAnimationProcessor().getBone("head");

        EntityModelData extraData = (EntityModelData) customPredicate.getExtraDataOfType(EntityModelData.class).get(0);
        if (head != null) {
            head.setRotationX(extraData.headPitch * ((float) Math.PI / 180F));
            head.setRotationY(extraData.netHeadYaw * ((float) Math.PI / 180F));
        }
    }
}
