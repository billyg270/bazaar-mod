package uk.co.cablepost.bazaar.client.block.renderer;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.block.entity.BlockEntityRenderer;
import net.minecraft.client.render.block.entity.BlockEntityRendererFactory;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import uk.co.cablepost.bazaar.block.blockentity.BazaarFlowerPotBlockEntity;
import uk.co.cablepost.bazaar.mixin.FlowerPotBlockAccessor;

import java.util.Objects;

public class BazaarFlowerPotBlockEntityRenderer <T extends BazaarFlowerPotBlockEntity> implements BlockEntityRenderer<T> {

    public BazaarFlowerPotBlockEntityRenderer(BlockEntityRendererFactory.Context context) {
        super();
    }

    public Item lastItem;
    public BlockState blockState;

    @Override
    public void render(BazaarFlowerPotBlockEntity blockEntity, float tickDelta, MatrixStack matrices, VertexConsumerProvider vertexConsumers, int light, int overlay) {
        Item item = blockEntity.getStack(0).getItem();

        if(blockEntity.empty){
            item = ItemStack.EMPTY.getItem();
        }

        if(lastItem == null || !Objects.equals(item, lastItem)){
            lastItem = item;

            for(Block b : FlowerPotBlockAccessor.getContentToPotted().keySet()){
                if(item == b.asItem()){
                    blockState = FlowerPotBlockAccessor.getContentToPotted().get(b).getDefaultState();
                }
            }
        }

        matrices.push();
        matrices.scale(1.32f, 1.2f, 1.32f);
        matrices.translate(-0.122f, 0.04f, -0.122f);
        MinecraftClient.getInstance().getBlockRenderManager().renderBlockAsEntity(blockState, matrices, vertexConsumers, 255, OverlayTexture.DEFAULT_UV);
        matrices.pop();
    }
}
