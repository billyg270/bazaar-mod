package uk.co.cablepost.bazaar.client.entity.renderer;

import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.util.Identifier;
import software.bernie.geckolib3.renderers.geo.GeoEntityRenderer;
import uk.co.cablepost.bazaar.Bazaar;
import uk.co.cablepost.bazaar.client.entity.model.ChonkerFlowerPotGolemModel;
import uk.co.cablepost.bazaar.entity.custom.ChonkerFlowerPotGolemEntity;

public class ChonkerFlowerPotGolemRenderer extends GeoEntityRenderer<ChonkerFlowerPotGolemEntity> {
    public ChonkerFlowerPotGolemRenderer(EntityRendererFactory.Context renderManager) {
        super(renderManager, new ChonkerFlowerPotGolemModel());
        this.shadowRadius = 1.5f;
    }

    @Override
    public Identifier getTextureResource(ChonkerFlowerPotGolemEntity object) {
        return new Identifier(Bazaar.MOD_ID, "textures/entity/chonker_pot_golem_texture.png");
    }

    @Override
    public RenderLayer getRenderType(ChonkerFlowerPotGolemEntity animatable, float partialTick, MatrixStack poseStack, VertexConsumerProvider bufferSource, VertexConsumer buffer, int packedLight, Identifier texture) {
        return RenderLayer.getEntityTranslucent(texture);
    }

    @Override
    protected float getDeathMaxRotation(ChonkerFlowerPotGolemEntity animatable) {
        return 0f;
    }
}
