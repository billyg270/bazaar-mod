package uk.co.cablepost.bazaar.client.entity.model;

import net.minecraft.util.Identifier;
import software.bernie.geckolib3.model.AnimatedGeoModel;
import uk.co.cablepost.bazaar.Bazaar;
import uk.co.cablepost.bazaar.entity.custom.ChonkerFlowerPotGolemEntity;

public class ChonkerFlowerPotGolemModel extends AnimatedGeoModel<ChonkerFlowerPotGolemEntity> {
    @Override
    public Identifier getModelResource(ChonkerFlowerPotGolemEntity object) {
        return new Identifier(Bazaar.MOD_ID, "geo/chonker_pot_golem_model.geo.json");
    }

    @Override
    public Identifier getTextureResource(ChonkerFlowerPotGolemEntity object) {
        return new Identifier(Bazaar.MOD_ID, "textures/entity/chonker_pot_golem_texture.png");
    }

    @Override
    public Identifier getAnimationResource(ChonkerFlowerPotGolemEntity animatable) {
        return new Identifier(Bazaar.MOD_ID, "animations/chonker_pot_golem.animation.json");
    }
}
