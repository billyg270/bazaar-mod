package uk.co.cablepost.bazaar.client;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.rendering.v1.BlockEntityRendererRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.util.DyeColor;
import uk.co.cablepost.bazaar.Bazaar;
import uk.co.cablepost.bazaar.Networking.BazaarNetworkingRegistry;
import uk.co.cablepost.bazaar.entity.BazaarEntityRegister;
import uk.co.cablepost.bazaar.client.block.renderer.BazaarFlowerPotBlockEntityRenderer;
import uk.co.cablepost.bazaar.client.entity.renderer.ChonkerFlowerPotGolemRenderer;

@Environment(EnvType.CLIENT)
public class BazaarClient implements ClientModInitializer {
    @Override
    public void onInitializeClient() {
        for(DyeColor dyeColor : DyeColor.values()){
            BlockRenderLayerMap.INSTANCE.putBlock(Bazaar.FLAG_BLOCKS.get(dyeColor), RenderLayer.getCutout());
        }

        BlockRenderLayerMap.INSTANCE.putBlock(Bazaar.BAZAAR_POT_1.BLOCK, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(Bazaar.BAZAAR_POT_2.BLOCK, RenderLayer.getCutout());

        BlockEntityRendererRegistry.register(Bazaar.BAZAAR_POT_1.BLOCK_ENTITY, BazaarFlowerPotBlockEntityRenderer::new);
        BlockEntityRendererRegistry.register(Bazaar.BAZAAR_POT_2.BLOCK_ENTITY, BazaarFlowerPotBlockEntityRenderer::new);

        EntityRendererRegistry.register(Bazaar.CHONKER_FLOWER_POT_GOLEM_ENTITY, ChonkerFlowerPotGolemRenderer::new);

        BazaarEntityRegister.RegisterClient();
        BazaarNetworkingRegistry.RegisterS2CPackets();
    }
}
