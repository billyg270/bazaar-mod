package uk.co.cablepost.bazaar.item;

import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import uk.co.cablepost.bazaar.Bazaar;
import uk.co.cablepost.bazaar.entity.BazaarEntityRegister;

public class BazaarItemRegistry {
    public static final Item CAMELEON_SPAWN_EGG = new SpawnEggItem(BazaarEntityRegister.CAMELEON, 0x00780a, 0xfcba03, new FabricItemSettings().group(ItemGroup.MISC).maxCount(64));

    public static void Register() {
        Registry.register(Registry.ITEM, new Identifier(Bazaar.MOD_ID, "cameleon_spawn_egg"), CAMELEON_SPAWN_EGG);
    }
}
