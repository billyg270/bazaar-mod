package uk.co.cablepost.bazaar;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.rendering.v1.ColorProviderRegistry;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.block.Block;
import net.minecraft.block.Material;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.SpawnEggItem;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.DyeColor;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import software.bernie.geckolib3.GeckoLib;
import uk.co.cablepost.bazaar.Networking.BazaarNetworkingRegistry;
import uk.co.cablepost.bazaar.block.BazaarPot1Block;
import uk.co.cablepost.bazaar.block.BazaarPot2Block;
import uk.co.cablepost.bazaar.block.blockentity.BazaarPot1BlockEntity;
import uk.co.cablepost.bazaar.block.blockentity.BazaarPot2BlockEntity;
import uk.co.cablepost.bazaar.entity.BazaarEntityRegister;
import uk.co.cablepost.bazaar.block.FlagBlock;
import uk.co.cablepost.bazaar.entity.custom.ChonkerFlowerPotGolemEntity;
import uk.co.cablepost.bazaar.item.BazaarItemRegistry;
import uk.co.cablepost.bazaar.sounds.BazaarSoundRegister;
import uk.co.cablepost.bazaar.util.MiniMadnessBlockEntity;

import java.util.HashMap;
import java.util.Map;

public class Bazaar implements ModInitializer {

    public static String MOD_ID = "bazaar";
    public static Map<DyeColor, FlagBlock> FLAG_BLOCKS = new HashMap<>();

    public static final MiniMadnessBlockEntity<BazaarPot1BlockEntity> BAZAAR_POT_1 = new MiniMadnessBlockEntity<>(
            new Identifier(MOD_ID, "bazaar_pot_1"),
            new BazaarPot1Block(FabricBlockSettings.of(Material.METAL).strength(4.0f)),
            BazaarPot1BlockEntity::new
    );

    public static final MiniMadnessBlockEntity<BazaarPot2BlockEntity> BAZAAR_POT_2 = new MiniMadnessBlockEntity<>(
            new Identifier(MOD_ID, "bazaar_pot_2"),
            new BazaarPot2Block(FabricBlockSettings.of(Material.METAL).strength(4.0f)),
            BazaarPot2BlockEntity::new
    );

    public static <T extends BlockEntity> BlockEntityType<T> registerBlockEntity(Identifier identifier, Block block, FabricBlockEntityTypeBuilder.Factory<T> blockEntityFactory){
        BlockEntityType<T> toRet = Registry.register(
                Registry.BLOCK_ENTITY_TYPE,
                identifier,
                FabricBlockEntityTypeBuilder.create(blockEntityFactory, block).build()
        );

        Registry.register(Registry.BLOCK, identifier, block);
        Registry.register(Registry.ITEM, identifier, new BlockItem(block, new FabricItemSettings().group(ItemGroup.DECORATIONS)));

        return toRet;
    }

    public static final EntityType<ChonkerFlowerPotGolemEntity> CHONKER_FLOWER_POT_GOLEM_ENTITY = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier(MOD_ID, "chonker_flower_pot_golem"),
            FabricEntityTypeBuilder
                    .create(SpawnGroup.CREATURE, ChonkerFlowerPotGolemEntity::new)
                    .dimensions(EntityDimensions.fixed(2.7f, 3.1f))
                    .build()
    );

    public static final Item CHONKER_FLOWER_POT_GOLEM_SPAWN_EGG = new SpawnEggItem(
            CHONKER_FLOWER_POT_GOLEM_ENTITY,
            0x9e8d64,
            0x43291e,
            new FabricItemSettings().group(ItemGroup.MISC)
    );

    @Override
    public void onInitialize() {

        // === Flags ===

        for(DyeColor dyeColor : DyeColor.values()){
            String id = dyeColor.name().toLowerCase() + "_flag";
            FlagBlock flagBlock = new FlagBlock(FabricBlockSettings.of(Material.WOOL).noCollision().breakInstantly().sounds(BlockSoundGroup.WOOL));
            FLAG_BLOCKS.put(dyeColor, flagBlock);

            int colour = dyeColor.getSignColor();

            if(colour == 0){
                colour = 0x404040;
            }

            int finalColour = colour;

            Registry.register(Registry.BLOCK, new Identifier(MOD_ID, id), flagBlock);
            ColorProviderRegistry.BLOCK.register((state, view, pos, tintIndex) -> finalColour, flagBlock);

            BlockItem flagItem = new BlockItem(flagBlock, new FabricItemSettings().group(ItemGroup.DECORATIONS));
            Registry.register(Registry.ITEM, new Identifier(MOD_ID, id), flagItem);
            ColorProviderRegistry.ITEM.register((view, tintIndex) -> finalColour, flagItem);
        }

        // === Mobs ===

        GeckoLib.initialize();

        FabricDefaultAttributeRegistry.register(CHONKER_FLOWER_POT_GOLEM_ENTITY, ChonkerFlowerPotGolemEntity.setAttributes());
        Registry.register(Registry.ITEM, new Identifier(MOD_ID, "chonker_flower_pot_golem_spawn_egg"), CHONKER_FLOWER_POT_GOLEM_SPAWN_EGG);
        BazaarEntityRegister.Register();
        BazaarItemRegistry.Register();
        BazaarSoundRegister.Register();
        BazaarNetworkingRegistry.RegisterC2SPackets();
    }
}

